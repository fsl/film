/*  glimGls.cc

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <sstream>

#include "utils/log.h"
#include "NewNifti/NewNifti.h"
#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "fslsurface/fslsurface.h"

#include "glimGls.h"
#include "miscmaths/t2z.h"
#include "miscmaths/f2z.h"

using namespace std;
using namespace NiftiIO;
using namespace Utilities;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;
using namespace fslsurface_name;

namespace FILM {

  GlimGls::GlimGls(const int pnumTS, const int psizeTS, const int pnumParams, const int pnContrasts, const int pnFContrasts, const int dofAdjustment) :
    numTS(pnumTS),
    sizeTS(psizeTS),
    nContrasts(pnContrasts),
    nFContrasts(pnFContrasts),
    numParams(pnumParams),
    b(numParams, numTS),
    copes(nContrasts, numTS),
    varcopes(nContrasts, numTS),
    fstats(nFContrasts,numTS),
    sigmaSquareds(numTS),
    dof(sizeTS - numParams + dofAdjustment)
    {
      I=IdentityMatrix(sizeTS);
      fDof.resize(nFContrasts,0);
    }

  void GlimGls::CleanUp()
  {
    copes.CleanUp();
    varcopes.CleanUp();
    sigmaSquareds.CleanUp();
    b.CleanUp();
    r.CleanUp();
  }

 void GlimGls::setData(const ColumnVector& y, const Matrix& x, const int ind, const Matrix& tContrasts, const Matrix& fContrasts)
 {
      // compute b
      Matrix inv_xx = MISCMATHS::pinv(x.t()*x);
      b.Column(ind) = inv_xx*x.t()*y;
      // compute r
      Matrix R = I-x*inv_xx*x.t();
      r = R*y;

      sigmaSquareds(ind) = (r.t()*r).AsScalar()/R.Trace();
      for ( int tContrast=1;tContrast <= tContrasts.Nrows();tContrast++) {
	Matrix con=tContrasts.Row(tContrast);
	copes(tContrast,ind)=(con*b.Column(ind)).AsScalar();
	double scale((con*inv_xx*con.t()).AsScalar());
	if ( scale <= 0 && con.MaximumAbsoluteValue() > 0 )
	  cerr << "Neff Error" << endl;
	varcopes(tContrast,ind)=sigmaSquareds(ind)*scale;
      }
      for ( int fContrast=1;fContrast <= fContrasts.Nrows();fContrast++) {
	Matrix fullFContrast( 0, tContrasts.Ncols() );
	for (int tContrast=1; tContrast<=fContrasts.Ncols() ; tContrast++ )
	  if (fContrasts(fContrast,tContrast)==1) fullFContrast &= tContrasts.Row(tContrast);
	fDof[fContrast-1]=fullFContrast.Nrows();
	fstats(fContrast,ind) = (b.Column(ind).t()*fullFContrast.t()*(fullFContrast*inv_xx*fullFContrast.t()*sigmaSquareds(ind)).i()*fullFContrast*b.Column(ind)).AsScalar()/(float)fullFContrast.Nrows();
      }
      // set corrections  SetCorrection(inv_xx, ind);
 }


    void GlimGls::Save(const volume<float>& mask, volume4D<float>& saveVolume, fslSurface<float, unsigned int>& saveSurface, const string& saveMode, const float reftdim)
    {
      // Need to save b, sigmaSquareds, corrections and dof
      Log& logger = LogSingleton::getInstance();
      for(int i = 1; i <= numParams; i++) //Beta
	  saveData(logger.getDir() + "/pe" + num2str(i),b.Row(i),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_ESTIMATE,saveSurface,saveMode);



      ColumnVector zstat(numTS);
      for ( int tContrast=1;tContrast <= nContrasts;tContrast++) {
	  saveData(logger.getDir() + "/cope" + num2str(tContrast),copes.Row(tContrast),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_ESTIMATE,saveSurface,saveMode);
	  saveData(logger.getDir() + "/varcope" + num2str(tContrast),varcopes.Row(tContrast),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_ESTIMATE,saveSurface,saveMode);
	  RowVector tstat(SD(copes.Row(tContrast),sqrt(varcopes.Row(tContrast))));
	  T2z::ComputeZStats(varcopes.Row(tContrast).AsColumn(), copes.Row(tContrast).AsColumn(), dof, zstat);
	  saveData(logger.getDir() + "/tstat" + num2str(tContrast),tstat,saveVolume,mask,true,false,-1,true,NIFTI_INTENT_TTEST,saveSurface,saveMode);
	  saveData(logger.getDir() + "/zstat" + num2str(tContrast),zstat.AsRow(),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_ZSCORE,saveSurface,saveMode);
      }
      for ( int fContrast=1;fContrast <= nFContrasts;fContrast++) {
	  saveData(logger.getDir() + "/fstat" + num2str(fContrast),fstats.Row(fContrast),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_FTEST,saveSurface,saveMode);
	  F2z::ComputeFStats(fstats.Row(fContrast).AsColumn(), fDof[fContrast-1], dof, zstat);
	  saveData(logger.getDir() + "/zfstat" + num2str(fContrast),zstat.AsRow(),saveVolume,mask,true,false,-1,true,NIFTI_INTENT_ZSCORE,saveSurface,saveMode);
      }
      saveData(logger.getDir() + "/sigmasquareds",sigmaSquareds,saveVolume,mask,true,false,-1,false,-1,saveSurface,saveMode);
      ColumnVector dofVec(1);
      dofVec = dof;
      write_ascii_matrix(logger.appendDir("dof"), dofVec);
    }

  void GlimGls::saveData(const string& outputName, const Matrix& data, volume4D<float>& saveVolume, const volume<float>& volumeMask, const  bool setVolumeRange, const bool setVolumeTdim, const float outputTdim, const bool setIntent, const int intentCode, fslSurface<float, unsigned int>& saveSurface, const string& saveMode)
  {
    if ( saveMode=="surface" ) {
      saveSurface.reinitialiseScalars(data.Nrows());
      for(unsigned int vertex=0; vertex < saveSurface.getNumberOfVertices(); vertex++)
	for(unsigned int timepoint=0; timepoint < saveSurface.getNumberOfScalarData(); timepoint++)
           saveSurface.setScalar(timepoint,vertex,data(timepoint+1,vertex+1));
      writeGIFTI(saveSurface,outputName+".func.gii",GIFTI_ENCODING_B64GZ);
    } else {
      saveVolume.setmatrix(data,volumeMask);
      if ( setVolumeTdim )
	saveVolume.settdim(outputTdim);
      if ( setIntent )
	saveVolume.set_intent(intentCode,0,0,0);
      if ( setVolumeRange )
	saveVolume.setDisplayMaximumMinimum(saveVolume.max(),saveVolume.min());
      save_volume4D(saveVolume,outputName);
    }
  }


}
