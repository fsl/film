/*  FilmOlsOptions.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */



#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

#include "armawrap/newmat.h"
#include "FilmOlsOptions.h"

using namespace std;
using namespace NEWMAT;

namespace FILM {

FilmOlsOptions* FilmOlsOptions::gopt = NULL;

void FilmOlsOptions::parse_command_line(int argc, char** argv, ofstream& logfile)
{

  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  logfile << "Arguments: " << endl;
  while (n<argc) {
    arg=argv[n];

    // output argument to logfile:
    logfile << arg << " ";

    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	gopt->inputfname = arg;
      else if(inp == 2)
	gopt->paradigmfname = arg;
      else if(inp == 3)
	gopt->thresh = atoi(argv[n]);
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
    } else if ( arg == "-v" ) {
      gopt->verbose = true;
      n++;
      continue;
    } else if ( arg == "-ar" ) {
      gopt->fitAutoRegressiveModel = true;
      n++;
      continue;
    } else if ( arg == "-gl" ) {
      gopt->globalEst = true;
      n++;
      continue;
    } else if ( arg == "-sa" ) {
      gopt->smoothACEst = true;
      n++;
      continue;
    } else if ( arg == "-det" ) {
      gopt->detrend = true;
      n++;
      continue;
    } else if ( arg == "-noest" ) {
      gopt->noest = true;
      n++;
      continue;
    }

    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }

    // put options with 1 argument here
    if ( arg == "-ms") {
      gopt->ms = atoi(argv[n+1]);
      logfile << argv[n+1] << " ";
      n+=2;
    } else if ( arg == "-rn" ) {
      gopt->datadir = argv[n+1];
      logfile << argv[n+1] << " ";
      n+=2;
    } else {
      cerr << "Unrecognised option " << arg << endl;
      n++;
    }
  }  // while (n<argc)

  logfile << endl << "---------------------------------------------" << endl << endl;

  if (gopt->inputfname.size()<1) {
    print_usage(argc,argv);
    throw Exception("Input filename not found");
  }
  if (gopt->paradigmfname.size()<1) {
    print_usage(argc,argv);
    throw Exception("Paradigm filename not found");
  }
  if (gopt->thresh < 0) {
    print_usage(argc,argv);
    throw Exception("Invalid thresh");
  }
}

void FilmOlsOptions::print_usage(int argc, char *argv[])
{
  cout << "Usage: " << argv[0] << " [options] <groupfile> <paradigmfile> <thresh>\n\n"
       << "  Available options are:\n"
       << "        -sa                                (smooths auto corr estimates)\n"
       << "        -ms <num>                          (susan mask size)\n"
       << "        -v                                 (outputs full data)\n"
       << "        -ar                                (fits autoregressive model - default "
       << "is to estimate direct from raw autocorr estimate)\n"
       << "        -gl                                (use global estimate only)\n"
       << "        -pw                                (prewhiten)\n"
       << "        -noest                             (do not estimate auto corrs)\n"
       << "        -det                               (detrend data as preprocessing step)\n"
       << "        -rn <dir>                          (directory name to store results in, default is "
       << gopt->datadir << ")\n"
       << "        -help\n\n";
}

}
