 /*  glimGls.h

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__glimgls_h)
#define __glimgls_h

#include <iostream>
#include <fstream>

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "fslsurface/fslsurfaceio.h"


namespace FILM {

#define BATCHSIZE 50

  class GlimGls
    {
    public:
      GlimGls(const int pnumTS, const int psizeTS, const int pnumParams, const int pnContrasts, const int pnFContrasts, const int dofAdjustment);

      void setData(const NEWMAT::ColumnVector& p_y, const NEWMAT::Matrix& p_x, const int ind, const NEWMAT::Matrix& tContrasts, const NEWMAT::Matrix& fContrasts);
      void Save(const NEWIMAGE::volume<float>& mask, NEWIMAGE::volume4D<float>& saveVolume,fslsurface_name::fslSurface<float, unsigned int>& saveSurface,const std::string& saveMode,const float reftdim=1.0);
      void saveData(const std::string& outputName, const NEWMAT::Matrix& data, NEWIMAGE::volume4D<float>& saveVolume, const NEWIMAGE::volume<float>& volumeMask, const  bool setVolumeRange, const bool setVolumeTdim, const float outputTdim, const bool setIntent, const int intentCode,  fslsurface_name::fslSurface<float, unsigned int>& saveSurface, const std::string& saveToVolume);
      NEWMAT::ColumnVector& getResiduals() { return r; }
      void CleanUp();

    private:

      GlimGls(const GlimGls&);
      GlimGls& operator=(const GlimGls& p_glimgls);

      int numTS;
      int sizeTS;
      int nContrasts;
      int nFContrasts;
      std::vector<int> fDof;
      int numParams;

      // Data to be saved:
      NEWMAT::Matrix b;
      NEWMAT::Matrix copes;
      NEWMAT::Matrix varcopes;
      NEWMAT::Matrix fstats;
      NEWMAT::RowVector sigmaSquareds;
      float dof;
      NEWMAT::ColumnVector r;

      NEWMAT::DiagonalMatrix I;
    };

}

#endif
