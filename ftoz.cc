/*  ftoz.cc

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <string>

#include "NewNifti/NewNifti.h"
#include "armawrap/newmat.h"
#include "miscmaths/f2z.h"
#include "newimage/newimageall.h"

using namespace std;
using namespace NiftiIO;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

// the real defaults are provided in the function parse_command_line

class globaloptions {
public:
  string fsfname;
  string doffile1;
  string doffile2;
  string zscoresfname;
public:
  globaloptions();
  ~globaloptions() {};
};

globaloptions globalopts;


globaloptions::globaloptions()
{
  // set up defaults
  zscoresfname = "zstats";
}

void print_usage(int argc, char *argv[])
{
  cout << "Usage: " << argv[0] << " [options] <fsfile> <dof1> <dof2> \n"
       << "  Available options are:\n"
       << "        -zout <outputvol>            (default is "
                                        << globalopts.zscoresfname << ")\n"
       << "        -help\n\n";
}

void parse_command_line(int argc, char* argv[])
{
  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  while (n<argc) {
    arg=argv[n];
    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	globalopts.fsfname = arg;
      else if(inp == 2)
	globalopts.doffile1 = argv[n];
      else if(inp == 3)
	globalopts.doffile2 = argv[n];
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
     }

    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }

    // put options with 1 argument here
    if ( arg == "-zout") {
      globalopts.zscoresfname = argv[n+1];
      n+=2;
    } else {
      cerr << "Unrecognised option " << arg << endl;
      n++;
    }
  }  // while (n<argc)

  if (globalopts.fsfname.size()<1) {
    cerr << "Fs filename not found\n\n";
    print_usage(argc,argv);
    exit(2);
  }
}

int main(int argc,char *argv[])
{

  try{
    parse_command_line(argc, argv);

    volume4D<float> input;
    ColumnVector fs,dof1,dof2;

    read_volume4D(input,globalopts.fsfname.c_str());
    fs=input.matrix().AsColumn();
    int numTS = fs.Nrows();
    cerr << numTS << endl;

    if(FslFileExists(globalopts.doffile1.c_str())) read_volume4D(input,globalopts.doffile1);
    else input = atof(globalopts.doffile1.c_str());
    dof1=input.matrix().AsColumn();
    if(FslFileExists(globalopts.doffile2.c_str())) read_volume4D(input,globalopts.doffile2);
    else input = atof(globalopts.doffile2.c_str());
    dof2=input.matrix().AsColumn();

    ColumnVector zs(numTS);
    F2z::ComputeFStats(fs, dof1, dof2, zs);

    input.setmatrix(zs.AsRow());
    input.setDisplayMaximumMinimum(input.max(),input.min());
    input.set_intent(NIFTI_INTENT_ZSCORE,0,0,0);
    save_volume4D(input,globalopts.zscoresfname);
  }

  catch(Exception p_excp)
    {
      cerr << p_excp.what() << endl;
      throw;
    }
  catch(...)
    {
      cerr << "Image error" << endl;
      throw;
    }
}
