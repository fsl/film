/*  FilmGlsOptions.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include <string>
#include <iostream>

#include "FilmGlsOptions.h"

using namespace std;
using namespace Utilities;

namespace FILM {

FilmGlsOptions* FilmGlsOptions::gopt = NULL;

  void FilmGlsOptions::parse_command_line(int argc, char** argv,Log& logger) {
    //Do the parsing;
    try {
      for(int a = options.parse_command_line(argc, argv); a < argc; a++);
      if( help.value() || !options.check_compulsory_arguments() ) {
	options.usage();
	exit(2);
      }
    }
    catch(X_OptionError& e) {
      cerr<<e.what()<<endl;
      cerr<<"try: film_gls --help"<<endl;
      exit(1);
    }
    if ( gopt->pava.set() + gopt->fitAutoRegressiveModel.set() + gopt->multitapersize.set() + gopt->tukeysize.set() > 1 ) {
      cerr << "Error: Only one autocorrelation method can be selected." << endl;
      exit(1);
    }
    if ( ( gopt->pava.set() || gopt->fitAutoRegressiveModel.set() || gopt->multitapersize.set() || gopt->tukeysize.set() ) && gopt->noest.set() ) {
      cerr << "Error: Autocorrelation estimation is off, but an autocorrelation mode is selected." << endl;
      exit(1);
    }



    logger.makeDir(gopt->datadir.value());
    cout << "Log directory is: " << logger.getDir() << endl;
    for(int a = 0; a < argc; a++)
      logger.str() << argv[a] << " ";
    logger.str() << endl << "---------------------------------------------" << endl << endl;
  }
}
