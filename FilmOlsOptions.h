/*  FilmOlsOptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__FilmOlsOptions_h)
#define __FilmOlsOptions_h

#include <string>
#include <math.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

namespace FILM {

class FilmOlsOptions {
 public:
  static FilmOlsOptions& getInstance();
  ~FilmOlsOptions() { delete gopt; }

  std::string inputfname;
  std::string paradigmfname;
  std::string datadir;
  std::string epifname;

  bool verbose;
  bool detrend;
  bool smoothACEst;
  bool fitAutoRegressiveModel;
  bool globalEst;
  bool prewhiten;
  bool noest;

  int trimscans;
  int thresh;
  int ms;
  int epith;
  short maxshort;

  void parse_command_line(int argc, char** argv, std::ofstream& logfile);

 private:
  FilmOlsOptions();

  const FilmOlsOptions& operator=(FilmOlsOptions&);
  FilmOlsOptions(FilmOlsOptions&);

  static FilmOlsOptions* gopt;

  void print_usage(int argc, char *argv[]);

};

inline FilmOlsOptions& FilmOlsOptions::getInstance(){
  if(gopt == NULL)
    gopt = new FilmOlsOptions();

  return *gopt;
}

inline FilmOlsOptions::FilmOlsOptions()
{
  // set up defaults
  datadir = "results";
  epifname = "epivolume";
  inputfname = "";
  paradigmfname = "";

  thresh = 0;
  ms = 4;
  epith = 100;
  maxshort = 32000;
  fitAutoRegressiveModel = false;

  prewhiten = false;
  smoothACEst = false;
  verbose = false;
  detrend = false;
  globalEst = false;
  noest = false;
}

}

#endif
