/*  glm.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__glm_h)
#define __glm_h

#include <iostream>
#include <fstream>
#define WANT_STREAM
#define WANT_MATH

#include "armawrap/newmat.h"

using namespace NEWMAT;
namespace FILM {

#define BATCHSIZE 50

  class Glm
    {
    public:
      Glm() : y(NULL),
	x(NULL),
	contrasts(NULL) {}

      void setData(Matrix& p_y, Matrix& p_x, Matrix& p_contrasts);

      const Matrix& ComputeResids();
      const float ComputeVar();
      const float Computecb();
      const ColumnVector& Computeb();

      const ColumnVector& Getb() const {return b;}

      const Matrix& Getr() const {return r;}

      void SetContrast(const int p_num) {c = contrasts->Row(p_num).t();}

    private:
      Glm(const Glm&);
      Glm& operator=(const Glm& p_glm);

      // y = bx + r
      Matrix* y;
      Matrix* x;

      // Contrast
      Matrix* contrasts;
      ColumnVector c;

      int numTS;
      int sizeTS;

      Matrix r;

      Matrix inv_xx;

      float cb;

      ColumnVector b;

      float var;

    };

}

#endif
