/*  ContrastMgr.h

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__ContrastMgr_h)
#define __ContrastMgr_h

#include <iostream>
#include <fstream>

#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"

#include "paradigm.h"

namespace FILM {

  class ContrastMgr
    {
    public:
      ContrastMgr();

      void run();

    private:
      ContrastMgr(const ContrastMgr&);
      ContrastMgr& operator=(const ContrastMgr& p_ContrastMgr);

    protected:

      void Load();
      void SaveFContrast(const std::string& suffix);
      void SaveTContrast(const std::string& suffix);

      void ComputeVarCope();
      void ComputeCope();
      void ComputeNeff();
      void ComputeZStat();
      void ComputeFStat();

      const NEWMAT::ColumnVector& GetVarCope() const {return varcb;}
      const NEWMAT::ColumnVector& GetCope() const {return cb;}
      const NEWMAT::ColumnVector& GetDof() const {return dof;}

      void SetTContrast(const int p_num, const int p_c_counter)
	{
	  tc = parad.getTContrasts().Row(p_num).t();
	  c_counter = p_c_counter;
	  contrast_num = p_num;
	}

      void SetFContrast(const int p_num, const int p_c_counter);

      void GetCorrection(NEWMAT::Matrix& corr, const int ind);

      // Contrasts:
      NEWMAT::ColumnVector tc;
      NEWMAT::Matrix fc;
      int c_counter;
      int numParams;
      int num_Ccontrasts_in_Fcontrast;
      bool contrast_valid;
      int contrast_num;

      Paradigm parad;

      // Loaded data:
      NEWMAT::Matrix corrections;
      NEWMAT::Matrix b;
      NEWMAT::ColumnVector dof;
      NEWMAT::ColumnVector sigmaSquareds;

      // Calculated data:
      NEWMAT::ColumnVector varcb;
      NEWMAT::ColumnVector cb;
      NEWMAT::ColumnVector neff;
      NEWMAT::ColumnVector tstat;
      NEWMAT::ColumnVector fstat;
      NEWMAT::ColumnVector zstat;

      // Other:
      int numTS;
      NEWIMAGE::volume<float> mask;
      bool is_avw_corrections;
    };

}

#endif
