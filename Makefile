include ${FSLCONFDIR}/default.mk

PROJNAME = film
XFILES   = film_gls contrast_mgr ttoz ftoz ttologp
SOFILES  = libfsl-film.so
SCRIPTS  = film_cifti
LIBS     = -lfsl-surface -lfsl-vtkio -lfsl-meshclass \
           -lfsl-first_lib -lfsl-newimage -lfsl-miscmaths -lfsl-utils \
           -lfsl-giftiio -lfsl-NewNifti -lfsl-znz -lfsl-cprob -lexpat
OBJS     = ContrastMgrOptions.o ContrastMgr.o gaussComparer.o \
           AutoCorrEstimator.o glm.o paradigm.o FilmOlsOptions.o \
           FilmGlsOptionsRes.o FilmGlsOptions.o glimGls.o

all: ${SOFILES} ${XFILES}

libfsl-film.so: ${OBJS}
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

%: %.o libfsl-film.so
	${CXX} ${CXXFLAGS} -o $@ $< -lfsl-film ${LDFLAGS}
