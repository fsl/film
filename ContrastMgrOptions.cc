/*  ContrastMgrOptions.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#define WANT_STREAM
#define WANT_MATH

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/log.h"
#include "armawrap/newmat.h"
#include "ContrastMgrOptions.h"

using namespace std;
using namespace NEWMAT;
using namespace Utilities;

namespace FILM {

ContrastMgrOptions* ContrastMgrOptions::gopt = NULL;

void ContrastMgrOptions::parse_command_line(int argc, char** argv, Log& logger)
{
  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  while (n<argc) {
    arg=argv[n];

    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	gopt->datadir = arg;
      else if(inp == 2)
	gopt->contrastfname = arg;
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
    } else if ( arg == "-v" ) {
      gopt->verbose = true;
      n++;
      continue;
    } else if ( arg == "-d" ) {
      gopt->debug = true;
      n++;
      continue;
    }

    if ( arg == "-cope") {
      gopt->copenumber = atoi(argv[n+1]);
      n+=2;
      continue;
    }
    else if ( arg == "-suffix") {
      gopt->suffix = argv[n+1];
      n+=2;
      continue;
    }
    else if (arg == "-f") {
      gopt->fcontrastfname = argv[n+1];
      n+=2;
      continue;
    }

    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }
  }  // while (n<argc)

  logger.setDir(gopt->datadir,"cmlogfile");
  for(int a = 0; a < argc; a++)
    logger.str() << argv[a] << " ";
  logger.str() << endl << "---------------------------------------------" << endl << endl;

  if (gopt->datadir.size()<1) {
    print_usage(argc,argv);
    throw Exception("Data directory not found");
  }
  if (gopt->contrastfname.size()<1) {
    print_usage(argc,argv);
    throw Exception("Contrast filename not found");
  }

}

void ContrastMgrOptions::print_usage(int argc, char *argv[])
{

  cout << "Usage: " << argv[0] << " [options] <datadir> <tcontrastfile>\n\n"
       << "  Available options are:\n"
       << "        -v                                 (outputs full data)\n"
       << "        -cope <num>                        (contrast number to start labelling copes from, default is 1)\n"
       << "        -suffix <string>                   (suffix to put on the end of the cope filename before the contrast number, default is nothing)\n"
       << "        -f <fcontrastfile>                 (fcontrast file)\n"
       << "        -d                                 (debug)\n"
       << "        -help\n\n";
}

#ifndef NO_NAMESPACE
}
#endif
