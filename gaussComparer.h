/*  gaussComparer.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__gaussComparer_h)
#define __gaussComparer_h

#include <iostream>
#include <fstream>
#define WANT_STREAM
#define WANT_MATH

#include "miscmaths/histogram.h"
#include "armawrap/newmat.h"
#include "utils/log.h"

#include "cprob/libprob.h"

using namespace NEWMAT;
using namespace MISCMATHS;
using namespace Utilities;

namespace FILM {

  class GaussComparer
    {
    public:
      GaussComparer(ColumnVector& pzscores)
	: zscores(pzscores), histogram(zscores, -10.0F, 10.0F, 2000) {}

      void setup(){histogram.generate();} // sets up histogram

      float computeRatio(const float prob, Log& out); // returns ratio of tail size in zscores compared to a Gaussian distribution

    private:
      GaussComparer();
      const GaussComparer& operator=(GaussComparer&);
      GaussComparer(GaussComparer&);

      const ColumnVector& zscores;

      Histogram histogram;
    };

}

#endif
