/*  paradigm.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__paradigm_h)
#define __paradigm_h

#include <string>
#include <vector>
#include "newimage/newimageall.h"

namespace FILM {

  class Paradigm
    {
    public:

      Paradigm() :
	designMatrix(),
	doingVoxelwise(false),
	tcontrasts(0,0),
	fcontrasts(0,0)
	{}

      const Paradigm& operator=(Paradigm& par)
      {
	designMatrix = par.designMatrix;
	tcontrasts = par.tcontrasts;
	fcontrasts = par.fcontrasts;
	return *this;
      }

      Paradigm(Paradigm& par) { operator=(par); }

      // getters and setters:
      NEWMAT::Matrix getDesignMatrix();
      NEWMAT::Matrix getDesignMatrix(const long voxel,const NEWIMAGE::volume<float>& mask, const std::vector<int64_t>& labels);
      void setDesignMatrix(const NEWMAT::Matrix& pdesignMatrix) { designMatrix = pdesignMatrix; }
      void setDesignMatrix(const int nTimepoints) { designMatrix.ReSize(nTimepoints,1); designMatrix=1; }

      NEWMAT::Matrix& getTContrasts() { return tcontrasts; }
      NEWMAT::Matrix& getFContrasts() { return fcontrasts; }

      // useful functions:
      void load(const std::string& p_paradfname, const std::string& p_tcontrastfname, const std::string& p_contrastfname, bool p_blockdesign, int p_sizets);
      void loadVoxelwise(const std::vector<int>& VoxelwiseEvNumber, const std::vector<std::string>& VoxelwiseEvName, const NEWIMAGE::volume<float>& mask);

      ~Paradigm() {}

    private:
      NEWMAT::Matrix designMatrix;
      bool doingVoxelwise;
      NEWMAT::Matrix tcontrasts;
      NEWMAT::Matrix fcontrasts;
      std::vector<NEWMAT::Matrix> voxelwiseEv;
      std::vector<int> voxelwiseEvTarget;
      std::vector<int> voxelwiseMode; //0 same size as input, 1 input.Xx1x1 2 input.1xYx1 3 input.1x1xZ
    };

}

#endif
