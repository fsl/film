/*  FilmGlsOptionsRes.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "armawrap/newmat.h"
#include "FilmGlsOptionsRes.h"

using namespace std;
using namespace NEWMAT;

namespace FILM {

FilmGlsOptionsRes* FilmGlsOptionsRes::gopt = NULL;

void FilmGlsOptionsRes::parse_command_line(int argc, char** argv, ofstream& logfile)
{

  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  logfile << "Arguments: " << endl;
  while (n<argc) {
    arg=argv[n];

    // output argument to logfile:
    logfile << arg << " ";

    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	gopt->inputfname = arg;
      else if(inp == 2)
	gopt->paradigmfname = arg;
      else if(inp == 3)
	gopt->thresh = atoi(argv[n]);
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
    } else if ( arg == "-v" ) {
      gopt->verbose = true;
      n++;
      continue;
    } else if ( arg == "-vdms" ) {
      gopt->verbose_dms = true;
      n++;
      continue;
    } else if ( arg == "-ar" ) {
      gopt->fitAutoRegressiveModel = true;
      gopt->tukey = false;
      n++;
      continue;
    } else if ( arg == "-pava" ) {
      gopt->pava = true;
      gopt->tukey = false;
      n++;
      continue;
    } else if ( arg == "-hfr" ) {
      gopt->highfreqremoval=true;
      n++;
      continue;
    } else if ( arg == "-sa" ) {
      gopt->smoothACEst = true;
      n++;
      continue;
    } else if ( arg == "-det" ) {
      gopt->detrend = true;
      n++;
      continue;
    } else if ( arg == "-noest" ) {
      gopt->noest = true;
      n++;
      continue;
    } else if ( arg == "-justpw" ) {
      gopt->justprewhiten = true;
      n++;
      continue;
    }



    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }

    // put options with 1 argument here
    if ( arg == "-ms") {
      gopt->ms = atoi(argv[n+1]);
      logfile << argv[n+1] << " ";
      n+=2;
    } else if ( arg == "-iters") {
      gopt->numiters = atoi(argv[n+1]);
      logfile << argv[n+1] << " ";
      n+=2;
    } else if ( arg == "-rn" ) {
      gopt->datadir = argv[n+1];
      logfile << argv[n+1] << " ";
      n+=2;
    } else if ( arg == "-tukey" ) {
      gopt->tukeysize = atoi(argv[n+1]);
      gopt->tukey = true;
      logfile << argv[n+1] << " ";
      n+=2;
    } else if ( arg == "-mt" ) {
      gopt->multitapersize = atoi(argv[n+1]);
      gopt->tukey = false;
      gopt->multitaper = true;
      logfile << argv[n+1] << " ";
      n+=2;
    }
    else if ( arg == "-epith" ) {
      gopt->epith = atoi(argv[n+1]);
      logfile << argv[n+1] << " ";
      n+=2;
    } else {
      cerr << "Unrecognised option " << arg << endl;
      n++;
    }
  }  // while (n<argc)

  logfile << endl << "---------------------------------------------" << endl << endl;

  if (gopt->inputfname.size()<1) {
    print_usage(argc,argv);
    throw Exception("Input filename not found");
  }
  if (gopt->paradigmfname.size()<1) {
    print_usage(argc,argv);
    throw Exception("Paradigm filename not found");
  }
  if (gopt->thresh < 0) {
    print_usage(argc,argv);
    throw Exception("Invalid thresh");
  }
}

void FilmGlsOptionsRes::print_usage(int argc, char *argv[])
{
  cout << "Usage: " << argv[0] << " [options] <groupfile> <paradigmfile> <thresh>\n\n"
       << "  Available options are:\n"
       << "        -zout <zscoresfile>                (default is "
       << gopt->zscoresfname << ")\n"
       << "        -sa                                (smooths auto corr estimates)\n"
       << "        -ms <num>                          (susan mask size)\n"
       << "        -iters <num>                       (number of prewhitening iterations to use - default is "
       << gopt->numiters << ")\n"
       << "        -hfr                               (perform high frequency artefact removal)\n"
       << "        -epith <num>                       (set susan brightness threshold - otherwise it is estimated)\n"
       << "        -v                                 (outputs full data)\n"
       << "        -vdms                              (outputs prewhiten design matrices)\n"
       << "        -ar                                (fits autoregressive model - default "
       << "is to use tukey with M=2*sqrt(numvols))\n"
       << "        -tukey <num>                       (uses tukey window to estimate autocorr with window size num - default "
       << "is to use tukey with M=2*sqrt(numvols))\n"
       << "        -mt <num>                          (uses multitapering with slepian tapers and num is the time-bandwidth product - default "
       << "is to use tukey with M=2*sqrt(numvols))\n"
       << "        -pava                              (estimates autocorr using PAVA - default "
       << "is to use tukey with M=2*sqrt(numvols))\n"
       << "        -noest                             (do not estimate auto corrs)\n"
       << "        -det                               (linear detrend data)\n"
       << "        -rn <dir>                          (directory name to store results in, default is "
       << gopt->datadir << ")\n"
       << "        -justpw                            (just prewhiten and output prewhitened data)\n"
       << "        -help\n\n";
}

}
