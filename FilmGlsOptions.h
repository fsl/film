/*  FilmGlsOptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__FilmGlsOptions_h)
#define __FilmGlsOptions_h
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace FILM {

class FilmGlsOptions {
 public:
  static FilmGlsOptions& getInstance();
  ~FilmGlsOptions() { delete gopt; }

  Utilities::Option<bool> ac_only;
  Utilities::Option<float> thresh;
  Utilities::Option<bool> fitAutoRegressiveModel;
  Utilities::Option<bool> help;
  Utilities::Option<bool> noest;
  Utilities::Option<bool> output_pwdata;
  Utilities::Option<bool> pava;
  Utilities::Option<bool> smoothACEst;
  Utilities::Option<bool> verbose;
  Utilities::Option<std::string> datadir;
  Utilities::Option<std::string> analysisMode;
  Utilities::Option<std::string> inputDataName;
  Utilities::Option<std::string> inputDataName2;
  Utilities::Option<std::string> meanInputFile;
  Utilities::Option<std::string> minimumTimepointFile;
  Utilities::Option<std::string> paradigmfname;
  Utilities::Option<std::string> contrastFile;
  Utilities::Option<std::string> fContrastFile;
  Utilities::Option<int> epith;
  Utilities::Option<int> ms;
  Utilities::Option<int> tukeysize;
  Utilities::Option<float> multitapersize;
  Utilities::Option<std::vector<int> > voxelwise_ev_numbers;
  Utilities::Option<std::vector<std::string> > voxelwiseEvFilenames;

  void parse_command_line(int argc, char** argv,Utilities::Log& logger);

private:
  FilmGlsOptions();
  const FilmGlsOptions& operator=(FilmGlsOptions&);
  FilmGlsOptions(FilmGlsOptions&);
  Utilities::OptionParser options;
  static FilmGlsOptions* gopt;
};

inline FilmGlsOptions& FilmGlsOptions::getInstance() {
   if(gopt == NULL)
     gopt = new FilmGlsOptions();
   return *gopt;
}


inline FilmGlsOptions::FilmGlsOptions() :
  ac_only(std::string("--ac"), false,
        std::string("\tperform autocorrelation estimation only"),
        false, Utilities::no_argument),
  thresh(std::string("--thr"),0,
	 std::string("~<num>\tinitial threshold to apply to input data"),
	 false, Utilities::requires_argument),
  fitAutoRegressiveModel(std::string("--ar"), false,
	std::string("\tfits autoregressive model - default is to use tukey with M=sqrt(numvols)"),
        false, Utilities::no_argument),
  help(std::string("--help"), false,
	std::string("\tprints this message"),
        false, Utilities::no_argument),
  noest(std::string("--noest"), false,
	std::string("\tdo not estimate auto corrs"),
	false, Utilities::no_argument),
  output_pwdata(std::string("--outputPWdata"), false,
	std::string("\toutput prewhitened data and average design matrix"),
	false, Utilities::no_argument),
  pava(std::string("--pava"), false,
	std::string("\testimates autocorr using PAVA - default is to use tukey with M=sqrt(numvols)"),
	false, Utilities::no_argument),
  smoothACEst(std::string("--sa"), false,
	std::string("\tsmooths auto corr estimates"),
	false, Utilities::no_argument),
  verbose(std::string("-v"), false,
	std::string("\toutputs full data"),
	false, Utilities::no_argument),
  datadir(std::string("--rn"), std::string("results"),
       std::string("~<file>\tdirectory name to store results in, default is results"),
       false, Utilities::requires_argument),
  analysisMode(std::string("--mode"), std::string("volumetric"),
       std::string("~<mode>\tanalysis mode, options are volumetric ( default ) or surface. Caution: surface-based functionality is still BETA"), false, Utilities::requires_argument),
  inputDataName(std::string("--in"), std::string(""),std::string("~<file>\tinput data file ( NIFTI for volumetric, GIFTI for surface )"),true, Utilities::requires_argument),
  inputDataName2(std::string("--in2"), std::string(""),std::string("~<file>\tinput surface for autocorr smoothing in surface-based analyses"),false, Utilities::requires_argument),
   meanInputFile(std::string("--mf"), std::string(""),
       std::string("~<file>\tre-estimate mean_func baseline - for use with perfusion subtraction"),
       false, Utilities::requires_argument),
   minimumTimepointFile(std::string("--mft"), std::string(""),
       std::string("~<file>\tminimum timepoint file"),
       false, Utilities::requires_argument),
   paradigmfname(std::string("--pd"), std::string(""),
       std::string("~<file>\tparadigm file"),
        false, Utilities::requires_argument),
   contrastFile(std::string("--con"), std::string(""),

       std::string("~<file>\tt-contrasts file"),
        false, Utilities::requires_argument),
   fContrastFile(std::string("--fcon"), std::string(""),
       std::string("~<file>\tf-contrasts file"),
        false, Utilities::requires_argument),

   epith(std::string("--epith"), 0,
        std::string("~<num>\tsusan brightness threshold for volumetric analysis/smoothing sigma for surface analysis"),
        false, Utilities::requires_argument),
   ms(std::string("--ms"), 4,
        std::string("~<num>\tsusan mask size for volumetric analysis/smoothing extent for surface analysis"),
        false, Utilities::requires_argument),
   tukeysize(std::string("--tukey"), 0,
        std::string("~<num>\tuses tukey window to estimate autocorr with window size num - default is to use tukey with M=sqrt(numvols)"),
        false, Utilities::requires_argument),
   multitapersize(std::string("--mt"), -1,
        std::string("~<num>\tuses multitapering with slepian tapers and num is the time-bandwidth product - default is to use tukey with M=sqrt(numvols)"),
        false, Utilities::requires_argument),
   voxelwise_ev_numbers(std::string("--ven"), std::vector<int>(),
         std::string("\tlist of numbers indicating voxelwise EVs position in the design matrix (list order corresponds to files in vxf option). Caution BETA option, only use with volumetric analysis."),
         false, Utilities::requires_argument),
  voxelwiseEvFilenames(std::string("--vef"), std::vector<std::string>(),
         std::string("\tlist of 4D images containing voxelwise EVs (list order corresponds to numbers in vxl option). Caution BETA option, only use with volumetric analysis."),
			false, Utilities::requires_argument),
  options("film_gls","film_gls")
   {
     try {
       options.add(ac_only);
       options.add(thresh);
       options.add(fitAutoRegressiveModel);
       options.add(help);
       options.add(noest);
       options.add(output_pwdata);
       options.add(pava);
       options.add(smoothACEst);
       options.add(verbose);
       options.add(datadir);
       options.add(analysisMode);
       options.add(inputDataName);
       options.add(inputDataName2);
       options.add(meanInputFile);
       options.add(minimumTimepointFile);
       options.add(paradigmfname);
       options.add(contrastFile);
       options.add(fContrastFile);
       options.add(epith);
       options.add(ms);
       options.add(tukeysize);
       options.add(multitapersize);
       options.add(voxelwise_ev_numbers);
       options.add(voxelwiseEvFilenames);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }

}
#endif
