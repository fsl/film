/*  AutoCorrEstimator.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__AutoCorrEstimator_h)
#define __AutoCorrEstimator_h

#include <iostream>
#include <fstream>

#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"
#include "fslsurface/fslsurface.h"

namespace FILM {

  class AutoCorrEstimator
    {
    public:
      AutoCorrEstimator(const NEWMAT::Matrix& pxdata) :
        sizeTS(pxdata.Nrows()),
	numTS(pxdata.Ncols()),
	xdata(pxdata),
	acEst(pxdata.Nrows(), pxdata.Ncols()),
	vrow(),
	xrow(),
	dummy(),
	realifft(),
	dm_mn(),
	zeropad(0)
	{
	  zeropad = MISCMATHS::nextpow2(pxdata.Nrows());
	  vrow.ReSize(zeropad);
	  xrow.ReSize(zeropad);
	  dummy.ReSize(zeropad);
	  realifft.ReSize(zeropad);
	  countLargeE.ReSize(zeropad);
	  countLargeE = 0;
	  acEst = 0;
	  acEst.Row(1) = 1;
	}

      void calcRaw(int lag = 0);
      void spatiallySmooth(const NEWMAT::ColumnVector& epivol, int masksize, double usanthresh, const NEWIMAGE::volume<float>& usan_vol, int lag=0);
      void spatiallySmooth(const fslsurface_name::fslSurface<float, unsigned int> surfaceData, const float sigma, const float extent, int lag=0);
      void applyConstraints();
      void filter(const NEWMAT::ColumnVector& filterFFT);
      NEWMAT::Matrix fitAutoRegressiveModel();
      void pava();
      void preWhiten(const NEWMAT::ColumnVector& in, NEWMAT::ColumnVector& ret, int i, NEWMAT::Matrix& dmret, bool highfreqremovalonly=false);
      void setDesignMatrix(const NEWMAT::Matrix& dm);
      double establishUsanThresh(const NEWMAT::ColumnVector& epivol);

      void getMeanEstimate(NEWMAT::ColumnVector& ret);

      NEWMAT::Matrix& getEstimates() { return acEst; }
      NEWMAT::Matrix& getE() { return E; }
      NEWMAT::ColumnVector& getCountLargeE(){ return countLargeE; }

      int getZeroPad() { return zeropad; }
      void tukey(int M);
      void multitaper(int M);
      int pacf(const NEWMAT::ColumnVector& x, int minorder, int maxorder, NEWMAT::ColumnVector& betas);

      NEWIMAGE::volume<float> mask;

    private:
      const int sizeTS;
      const int numTS;
      AutoCorrEstimator();
      const AutoCorrEstimator& operator=(AutoCorrEstimator&);
      AutoCorrEstimator(AutoCorrEstimator&);
      void getSlepians(int M, int sizeTS, NEWMAT::Matrix& slepians);

      const NEWMAT::Matrix& xdata;
      NEWMAT::Matrix acEst;
      NEWMAT::Matrix E;
      NEWMAT::ColumnVector countLargeE;

      NEWMAT::Matrix dminFFTReal;
      NEWMAT::Matrix dminFFTImag;

      NEWMAT::ColumnVector vrow;
      NEWMAT::ColumnVector xrow;

      NEWMAT::ColumnVector dm_fft_real, dm_fft_imag;
      NEWMAT::ColumnVector x_fft_real, ac_fft_real;
      NEWMAT::ColumnVector x_fft_im, ac_fft_im;

      NEWMAT::ColumnVector dummy;
      NEWMAT::ColumnVector realifft;
      NEWMAT::ColumnVector dm_mn;

      int zeropad;
    };

}

#endif
