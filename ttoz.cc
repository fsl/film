/*  ttoz.cc

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <string>

#include "NewNifti/NewNifti.h"
#include "armawrap/newmat.h"
#include "miscmaths/t2z.h"
#include "newimage/newimageall.h"


using namespace std;
using namespace NiftiIO;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

// the real defaults are provided in the function parse_command_line

class globaloptions {
public:
  string varsfname;
  string cbsfname;
  string doffile;

  string zscoresfname;
public:
  globaloptions();
  ~globaloptions() {};
};

globaloptions globalopts;


globaloptions::globaloptions()
{
  // set up defaults
  zscoresfname = "zstats";
}

void print_usage(int argc, char *argv[])
{
  cout << "Usage: " << argv[0] << " [options] <varsfile> <cbsfile> <dof> \n"
       << "  Available options are:\n"
       << "        -zout <outputvol>            (default is "
                                        << globalopts.zscoresfname << ")\n"
       << "        -help\n\n";
}

void parse_command_line(int argc, char* argv[])
{
  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  while (n<argc) {
    arg=argv[n];
    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	globalopts.varsfname = arg;
      else if(inp == 2)
	globalopts.cbsfname = arg;
      else if(inp == 3)
	globalopts.doffile = arg;
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
     }

    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }

    // put options with 1 argument here
    if ( arg == "-zout") {
      globalopts.zscoresfname = argv[n+1];
      n+=2;
    } else {
      cerr << "Unrecognised option " << arg << endl;
      n++;
    }
  }  // while (n<argc)

  if (globalopts.varsfname.size()<1) {
    cerr << "Vars filename not found\n\n";
    print_usage(argc,argv);
    exit(2);
  }
}

int main(int argc,char *argv[])
{
  try{
    parse_command_line(argc, argv);

    volume4D<float> input;
    ColumnVector varsm,cbsm,dofsm;

    read_volume4D(input,globalopts.cbsfname);
    cbsm=input.matrix().AsColumn();
    read_volume4D(input,globalopts.varsfname);
    varsm=input.matrix().AsColumn();

    int numTS = varsm.Nrows();

    if(FslFileExists(globalopts.doffile.c_str())) read_volume4D(input,globalopts.doffile);
    else input = atof(globalopts.doffile.c_str());
    dofsm=input.matrix().AsColumn();

    ColumnVector zs(numTS);
    T2z::ComputeZStats(varsm, cbsm, dofsm, zs);

    input.setmatrix(zs.AsRow());
    input.setDisplayMaximumMinimum(input.max(),input.min());
    input.set_intent(NIFTI_INTENT_ZSCORE,0,0,0);
    save_volume4D(input,globalopts.zscoresfname);
  }
  catch(Exception p_excp)
    {
      cerr << p_excp.what() << endl;
      throw;
    }
  catch(...)
    {
      cerr << "Image error" << endl;
      throw;
    }
}
