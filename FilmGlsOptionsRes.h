/*  FilmGlsOptionsRes.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__FilmGlsOptionsRes_h)
#define __FilmGlsOptionsRes_h

#include <string>
#include <math.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

namespace FILM {

class FilmGlsOptionsRes {
 public:
  static FilmGlsOptionsRes& getInstance();
  ~FilmGlsOptionsRes() { delete gopt; }

  std::string inputfname;
  std::string paradigmfname;
  std::string contrastfname;
  std::string epifname;
  std::string datadir;

  std::string neffsfname;
  std::string zscoresfname;

  bool verbose;
  bool verbose_dms;
  bool rand;
  bool detrend;
  bool smoothACEst;
  bool fitAutoRegressiveModel;
  bool tukey;
  bool pava;
  bool multitaper;
  bool highfreqremoval;
  bool justprewhiten;

  int numiters;
  int tukeysize;
  float multitapersize;
  bool noest;

  int trimscans;
  int thresh;
  int ms;
  int numrand;
  short maxshort;
  int epith;

  void parse_command_line(int argc, char** argv, std::ofstream& logfile);

 private:
  FilmGlsOptionsRes();

  const FilmGlsOptionsRes& operator=(FilmGlsOptionsRes&);
  FilmGlsOptionsRes(FilmGlsOptionsRes&);

  static FilmGlsOptionsRes* gopt;

  void print_usage(int argc, char *argv[]);

};

inline FilmGlsOptionsRes& FilmGlsOptionsRes::getInstance(){
  if(gopt == NULL)
    gopt = new FilmGlsOptionsRes();

  return *gopt;
}

inline FilmGlsOptionsRes::FilmGlsOptionsRes()
{
  // set up defaults
  zscoresfname = "zstats";
  neffsfname = "neffs";
  datadir = "results";
  epifname = "epivolume";
  thresh = 0;
  contrastfname = "";
  inputfname = "";
  paradigmfname = "";
  epith = 0;

  numrand = 0;
  ms = 4;
  maxshort = 32000;
  fitAutoRegressiveModel = false;
  tukeysize = 0;
  multitapersize = 4.0;

  highfreqremoval = false;
  numiters = 1;
  smoothACEst = false;
  verbose = false;
  verbose_dms = false;
  rand = false;
  detrend = false;
  pava = false;
  tukey = true;
  multitaper = false;
  noest = false;
  justprewhiten = false;

}

}

#endif
