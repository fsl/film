/*  ContrastMgrOptions.h

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(__ContrastMgrOptions_h)
#define __ContrastMgrOptions_h

#include <string>
#include <math.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/log.h"

namespace FILM {

class ContrastMgrOptions {
 public:
  static ContrastMgrOptions& getInstance();
  ~ContrastMgrOptions() { delete gopt; }

  std::string contrastfname;
  std::string fcontrastfname;
  std::string datadir;
  std::string neffsfname;
  std::string suffix;

  bool debug;
  bool verbose;
  short maxshort;
  short copenumber;

  void parse_command_line(int argc, char** argv, Utilities::Log& logger);

 private:
  ContrastMgrOptions();

  const ContrastMgrOptions& operator=(ContrastMgrOptions&);
  ContrastMgrOptions(ContrastMgrOptions&);

  static ContrastMgrOptions* gopt;

  void print_usage(int argc, char *argv[]);

};

inline ContrastMgrOptions& ContrastMgrOptions::getInstance(){
  if(gopt == NULL)
    gopt = new ContrastMgrOptions();

  return *gopt;
}

inline ContrastMgrOptions::ContrastMgrOptions()
{
  // set up defaults
  datadir = "";
  contrastfname = "";
  fcontrastfname = "";
  suffix = "";

  copenumber = 1;
  maxshort = 32000;
  verbose = false;
  debug = false;
}

}

#endif
