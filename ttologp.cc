/*  ttologp.cc

    Mark Woolrich and Matthew Webster, FMRIB Image Analysis Group

    Copyright (C) 1999-2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <fstream>
#include <string>

#include "NewNifti/NewNifti.h"
#include "armawrap/newmat.h"
#include "miscmaths/t2z.h"
#include "newimage/newimageall.h"


using namespace std;
using namespace NiftiIO;
using namespace NEWMAT;
using namespace MISCMATHS;
using namespace NEWIMAGE;

// the real defaults are provided in the function parse_command_line

class globaloptions {
public:
  string varsfname;
  string cbsfname;
  int dof;

  string logpfname;
public:
  globaloptions();
  ~globaloptions() {};
};

globaloptions globalopts;


globaloptions::globaloptions()
{
  // set up defaults
  logpfname = "logps";
}

void print_usage(int argc, char *argv[])
{
  cout << "Usage: " << argv[0] << " [options] <varsfile> <cbsfile> <dof> \n"
       << "  Available options are:\n"
       << "        -logpout <outputvol>            (default is "
                                        << globalopts.logpfname << ")\n"
       << "        -help\n\n";
}

void parse_command_line(int argc, char* argv[])
{
  if(argc<2){
    print_usage(argc,argv);
    exit(1);
  }

  int inp = 1;
  int n=1;
  string arg;
  char first;

  while (n<argc) {
    arg=argv[n];
    if (arg.size()<1) { n++; continue; }
    first = arg[0];
    if (first!='-') {
      if(inp == 1)
	globalopts.varsfname = arg;
      else if(inp == 2)
	globalopts.cbsfname = arg;
      else if(inp == 3)
	globalopts.dof = atoi(argv[n]);
      else
	{
	  cerr << "Mismatched argument " << arg << endl;
	  break;
	}
      n++;
      inp++;
      continue;
    }

    // put options without arguments here
    if ( arg == "-help" ) {
      print_usage(argc,argv);
      exit(0);
     }

    if (n+1>=argc)
      {
	cerr << "Lacking argument to option " << arg << endl;
	break;
      }

    // put options with 1 argument here
    if ( arg == "-logpout") {
      globalopts.logpfname = argv[n+1];
      n+=2;
    } else {
      cerr << "Unrecognised option " << arg << endl;
      n++;
    }
  }  // while (n<argc)

  if (globalopts.varsfname.size()<1) {
    cerr << "Vars filename not found\n\n";
    print_usage(argc,argv);
    exit(2);
  }
}

int main(int argc,char *argv[])
{
 try{
    parse_command_line(argc, argv);

    volume4D<float> input;
    ColumnVector vars,cbs;

    read_volume4D(input,globalopts.cbsfname);
    cbs=input.matrix().AsColumn();
    read_volume4D(input,globalopts.varsfname);
    vars=input.matrix().AsColumn();

    int numTS = vars.Nrows();

    ColumnVector ps(numTS);
    T2z::ComputePs(vars, cbs, globalopts.dof, ps);

    input.setmatrix(ps.AsRow());
    input.setDisplayMaximumMinimum(input.max(),input.min());
    input.set_intent(NIFTI_INTENT_LOGPVAL,0,0,0);
    save_volume4D(input,globalopts.logpfname.c_str());
  }

  catch(Exception p_excp)
    {
      cerr << p_excp.what() << endl;
      throw;
    }
  catch(...)
    {
      cerr << "Image error" << endl;
      throw;
    }
}
