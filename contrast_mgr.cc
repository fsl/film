/*  contrast_mgr.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
  cout << "Contrast_mgr is now deprecated, do not use this version on Post-stats only FEAT runs." << endl;
  return 0;
}
