/*  paradigm.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include <fstream>

#include "utils/log.h"
#include "armawrap/newmat.h"
#include "miscmaths/miscmaths.h"
#include "newimage/newimageall.h"

#include "paradigm.h"


using namespace std;
using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace MISCMATHS;
using namespace FILM;

namespace FILM {

  void Paradigm::load(const string& p_paradfname, const string& p_tcontrastfname, const string& p_fcontrastfname, bool p_blockdesign, int p_sizets)
    {
      if(p_paradfname != "")
	designMatrix=read_vest(p_paradfname);

      if(p_tcontrastfname != "")
	tcontrasts=read_vest(p_tcontrastfname);

      if(p_fcontrastfname != "")
	fcontrasts=read_vest(p_fcontrastfname);

      // Check time series match up with design
      if(p_paradfname != "" && designMatrix.Nrows() != p_sizets)
	{
	  cerr << "Num scans = "<< p_sizets << ", design matrix rows = " << designMatrix.Nrows() << endl;
	  throw Exception("size of design matrix does not match number of scans\n");
	}

      // Check contrasts match
      if(p_tcontrastfname != "" &&  p_fcontrastfname != "" && tcontrasts.Nrows() != fcontrasts.Ncols())
	{
	  cerr << "tcontrasts.Nrows()  = " << tcontrasts.Nrows() << endl;
	  cerr << "fcontrasts.Ncols()  = " << fcontrasts.Ncols() << endl;
	  throw Exception("size of tcontrasts does not match fcontrasts\n");
	}

      // Check contrast matches design matrix
      if(p_paradfname != "" &&  p_tcontrastfname != "" && designMatrix.Ncols() != tcontrasts.Ncols())
	 {
	  cerr << "Num tcontrast cols  = " << tcontrasts.Ncols() << ", design matrix cols = "
	    << designMatrix.Ncols() << endl;
	  throw Exception("size of design matrix does not match t contrasts\n");
	 }

      if(p_blockdesign)
	{
	  // Need to adjust amplitude from (-1,1) to (0,1)
	  designMatrix = (designMatrix + 1)/2;
	}
    }

}

void Paradigm::loadVoxelwise(const vector<int>& voxelwiseEvNumber, const vector<string>& voxelwiseEvName, const volume<float>& mask)
{
  volume4D<float> input;
  voxelwiseEvTarget=voxelwiseEvNumber;
  voxelwiseEv.resize(voxelwiseEvNumber.size());
  for(unsigned int i=0; i<voxelwiseEvNumber.size(); i++) {
    if(voxelwiseEvTarget[i]>designMatrix.Ncols())
      throw Exception("voxelwiseEvTarget is greater than number of design EVs)");
    read_volume4D(input,voxelwiseEvName.at(i));
    voxelwiseMode.push_back(-1);
    if ( samesize(input[0],mask) ) voxelwiseMode[i]=0;
    else if ( input.xsize() == mask.xsize() && input.ysize() == 1 && input.zsize() == 1 ) voxelwiseMode[i]=1;
    else if ( input.xsize() == 1 && input.ysize() == mask.ysize() && input.zsize() == 1 ) voxelwiseMode[i]=2;
    else if ( input.xsize() == 1 && input.ysize() == 1 && input.zsize() == mask.zsize() ) voxelwiseMode[i]=3;
    if ( voxelwiseMode[i]==0 )
      voxelwiseEv[i]=input.matrix(mask);
    else
      voxelwiseEv[i]=input.matrix();
  }
  doingVoxelwise=true;
}

NEWMAT::Matrix Paradigm::getDesignMatrix( )
{
  Matrix output=designMatrix;
  return output;
}

NEWMAT::Matrix Paradigm::getDesignMatrix(const long voxel, const volume<float>& mask, const vector<int64_t>& labels )
{
  Matrix output=designMatrix;
  if (doingVoxelwise)
    for (unsigned int ev=0; ev<voxelwiseEvTarget.size(); ev++)
    {
      if ( voxelwiseMode[ev]==0 )
	output.Column(voxelwiseEvTarget[ev])=voxelwiseEv[ev].Column(voxel);
      else {
	//cerr << voxel << " " << labels.size() << endl;
	//cerr << "Looking up voxel number:" << coordinates[0] << " " << coordinates[1] << " " << coordinates[2] << endl;
	//cerr << coordinates[voxelwiseMode[ev]-1]+1 << endl;
	vector<int64_t> coordinates=mask.labelToCoord(labels[voxel-1]);
	output.Column(voxelwiseEvTarget[ev])=voxelwiseEv[ev].Column(coordinates[voxelwiseMode[ev]-1]+1);
      }
    }
  return output;
}
