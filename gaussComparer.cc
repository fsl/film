/*  gaussComparer.cc

    Mark Woolrich, FMRIB Image Analysis Group

    Copyright (C) 1999-2000 University of Oxford  */

/*  CCOPYRIGHT  */

#include <iostream>

#include "gaussComparer.h"

using namespace std;

namespace FILM {

  float GaussComparer::computeRatio(const float prob, Log& out)
    {
      float theorZScore = -ndtri(prob);
      int sum = histogram.integrateToInf(theorZScore) + histogram.integrateFromInf(-theorZScore);

      out << "p = " << prob << ": Positive only, ratio =  " << ((float) histogram.integrateToInf(theorZScore)
					    /(float)histogram.integrateAll())/(prob) << endl;
      out << "p = " << prob << ": Negative only, ratio =  " << ((float) histogram.integrateFromInf(-theorZScore)
					    /(float)histogram.integrateAll())/(prob) << endl;

      return ((float)sum/(float)histogram.integrateAll())/(2*prob);
    }

#ifndef NO_NAMESPACE
}
#endif
